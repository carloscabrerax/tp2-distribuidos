/*Alumnos: Carlos Cabrera y Alejandro Aguilera
 */
import jdk.nashorn.internal.parser.JSONParser;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.SQLOutput;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
      int select;
      do {
        System.out.println("Selecciona la opción:");
        System.out.println("1-Agregar Persona, 2-Listar Personas, 3- Ver Asignaturas de una Persona, 4-Crear Asignatura, 5- Salir");

        Scanner scanner = new Scanner(System.in);
        select = scanner.nextInt();

        switch (select) {
          case 1:
            crearPersona();
            break;
          case 2:
            listarPersonas();
            break;
          case 3:
            verPersona(); //Como dato muestra las asignaturas pertenecientes a la persona
            break;
          case 4:
            crearAsignatura();
            break;
          case 5:
            System.out.println("Finalizado");
            break;
          default:
              System.out.println("Opción incorrecta: intenta nuevamente");
              break;
        }

      } while (select != 5);

    }
    private static void crearPersona(){
      Scanner scanner = new Scanner(System.in);
      System.out.println("Creación de Persona, ingrese los datos:");
      System.out.println("Cédula:");
      String cedula = scanner.next();
      System.out.println("Nombre:");
      String nombre = scanner.next();
      System.out.println("Apellido:");
      String apellido = scanner.next();
      String json = "{\"cedula\":\""+cedula+"\",\"nombre\":\""+nombre+"\",\"apellido\":\""+apellido+"\"}";
      //JSONObject jo = new JSONObject("{\"cedula\":\""+cedula+"\",\"nombre\":\""+nombre+"\",\"apellido\":\""+apellido+"\"}");
      try {
        URL url = new URL("http://localhost:8080/personas/rest/personas");
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestProperty("Accept", "application/json");
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestMethod("POST");
        conn.setDoOutput(true);

        try(OutputStream os = conn.getOutputStream()) {
          byte[] input = json.getBytes("utf-8");
          os.write(input, 0, input.length);
        }

        if (conn.getResponseCode() < 200  || conn.getResponseCode() >= 300 ) {
          throw new RuntimeException("Error HTTP - código: " + conn.getResponseCode()+" : "+conn.getResponseMessage());
        }

        BufferedReader br = new BufferedReader(new InputStreamReader(
          (conn.getInputStream())));

        String output;
        System.out.println("Impresión del contenido de la respuesta: \n");
        while ((output = br.readLine()) != null) {
          System.out.println(output);
        }

        conn.disconnect();
      } catch (IOException e) {
        e.printStackTrace();
      }

    }
    private static void crearAsignatura(){
    Scanner scanner = new Scanner(System.in);
    System.out.println("Creación de Asignatura, ingrese los datos:");
    System.out.println("Nombre:");
    String nombre = scanner.next();
    String json = "{\"nombre\":\""+nombre+"\"}";
    try {
      URL url = new URL("http://localhost:8080/personas/rest/asignaturas");
      HttpURLConnection conn = (HttpURLConnection) url.openConnection();
      conn.setRequestProperty("Accept", "application/json");
      conn.setRequestProperty("Content-Type", "application/json");
      conn.setRequestMethod("POST");
      conn.setDoOutput(true);

      try(OutputStream os = conn.getOutputStream()) {
        byte[] input = json.getBytes("utf-8");
        os.write(input, 0, input.length);
      }

      if (conn.getResponseCode() < 200  || conn.getResponseCode() >= 300 ) {
        throw new RuntimeException("Error HTTP - código: " + conn.getResponseCode()+" : "+conn.getResponseMessage());
      }

      BufferedReader br = new BufferedReader(new InputStreamReader(
        (conn.getInputStream())));

      String output;
      System.out.println("Impresión del contenido de la respuesta: \n");
      while ((output = br.readLine()) != null) {
        System.out.println(output);
      }

      conn.disconnect();
    } catch (IOException e) {
      e.printStackTrace();
    }

  }
    private static void listarPersonas() {
      try {
        URL url = new URL("http://localhost:8080/personas/rest/personas");
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestProperty("Accept", "application/json");
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestMethod("GET");

        if (conn.getResponseCode() < 200  || conn.getResponseCode() >= 300 ) {
          throw new RuntimeException("Error HTTP - código : " + conn.getResponseCode()+" : "+conn.getResponseMessage());
        }

        BufferedReader br = new BufferedReader(new InputStreamReader(
          (conn.getInputStream())));

        String output;
        System.out.println("Lista de personas:");
        while ((output = br.readLine()) != null) {
          JSONArray arr = new JSONArray(output);
          for (int i = 0; i < arr.length(); i++){
            JSONObject a = arr.getJSONObject(i);
            System.out.println("*" + a.getString("nombre")+" "+a.getString("apellido"));
          }
        }

        conn.disconnect();
      } catch (IOException e) {
        e.printStackTrace();
      }

    }
    private static void verPersona() {
      Scanner scanner = new Scanner(System.in);
      System.out.println("Ingrese la cedula de la persona:");
      String cedula = scanner.next();
    try {
      URL url = new URL("http://localhost:8080/personas/rest/personas/"+cedula);
      HttpURLConnection con = (HttpURLConnection) url.openConnection();
      con.setRequestProperty("Accept", "application/json");
      con.setRequestProperty("Content-Type", "application/json");
      con.setRequestMethod("GET");

      if (con.getResponseCode() < 200  || con.getResponseCode() >= 300 ) {
        throw new RuntimeException("Error HTTP - código: " + con.getResponseCode()+" : "+con.getResponseMessage());
      }

      BufferedReader br = new BufferedReader(new InputStreamReader(
        (con.getInputStream())));
      String output;
      System.out.println("Persona y Asignatura perteneciente a la persona: \n");
      while ((output = br.readLine()) != null) {
        JSONObject obj = new JSONObject(output);
        System.out.println(obj.getString("nombre")+" "+obj.getString("apellido"));
        System.out.println("******Asignaturas asociadas:*******");
          JSONArray arr = obj.getJSONArray("asignaturas");
          for (int i = 0; i < arr.length(); i++){
            JSONObject a = arr.getJSONObject(i);
            System.out.println(a.getString("nombre"));
          }
        System.out.println("***********************************");
      }
      con.disconnect();
    } catch (IOException e) {
      e.printStackTrace();
    }

  }
}
