--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.17
-- Dumped by pg_dump version 9.5.17

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: borrar_asignatura(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.borrar_asignatura() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    DELETE FROM asignatura_persona WHERE asignatura_id = OLD.id;
   RETURN OLD;
END;
$$;


ALTER FUNCTION public.borrar_asignatura() OWNER TO postgres;

--
-- Name: borrar_persona(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.borrar_persona() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    DELETE FROM asignatura_persona WHERE persona_id = OLD.cedula;
    RETURN OLD;
END;
$$;


ALTER FUNCTION public.borrar_persona() OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: asignatura; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.asignatura (
    id integer NOT NULL,
    nombre character varying(60) NOT NULL
);


ALTER TABLE public.asignatura OWNER TO postgres;

--
-- Name: asignatura_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.asignatura_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.asignatura_id_seq OWNER TO postgres;

--
-- Name: asignatura_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.asignatura_id_seq OWNED BY public.asignatura.id;


--
-- Name: asignatura_persona; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.asignatura_persona (
    asignatura_id integer NOT NULL,
    persona_id integer NOT NULL,
    id integer NOT NULL,
    calificacion integer
);


ALTER TABLE public.asignatura_persona OWNER TO postgres;

--
-- Name: asignatura_persona_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.asignatura_persona_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.asignatura_persona_id_seq OWNER TO postgres;

--
-- Name: asignatura_persona_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.asignatura_persona_id_seq OWNED BY public.asignatura_persona.id;


--
-- Name: persona; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.persona (
    cedula integer NOT NULL,
    nombre character varying(1000),
    apellido character varying(1000)
);


ALTER TABLE public.persona OWNER TO postgres;

--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.asignatura ALTER COLUMN id SET DEFAULT nextval('public.asignatura_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.asignatura_persona ALTER COLUMN id SET DEFAULT nextval('public.asignatura_persona_id_seq'::regclass);

ALTER TABLE ONLY public.asignatura_persona
    ADD CONSTRAINT asignatura_persona_pk PRIMARY KEY (id);

ALTER TABLE ONLY public.asignatura
    ADD CONSTRAINT asignatura_pk PRIMARY KEY (id);

ALTER TABLE ONLY public.persona
    ADD CONSTRAINT pk_cedula PRIMARY KEY (cedula);


--
-- Name: ap_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX ap_index ON public.asignatura_persona USING btree (asignatura_id, persona_id);


--
-- Name: asignatura_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX asignatura_id_uindex ON public.asignatura USING btree (id);


--
-- Name: asignatura_id_uindex_2; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX asignatura_id_uindex_2 ON public.asignatura USING btree (id);


--
-- Name: asignatura_persona_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX asignatura_persona_id_uindex ON public.asignatura_persona USING btree (id);


--
-- Name: asignatura_trigger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER asignatura_trigger BEFORE DELETE ON public.asignatura FOR EACH ROW EXECUTE PROCEDURE public.borrar_asignatura();


--
-- Name: persona_trigger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER persona_trigger BEFORE DELETE ON public.persona FOR EACH ROW EXECUTE PROCEDURE public.borrar_persona();


--
-- Name: asignatura_persona_asignatura_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.asignatura_persona
    ADD CONSTRAINT asignatura_persona_asignatura_id_fk FOREIGN KEY (asignatura_id) REFERENCES public.asignatura(id);


--
-- Name: asignatura_persona_persona_cedula_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.asignatura_persona
    ADD CONSTRAINT asignatura_persona_persona_cedula_fk FOREIGN KEY (persona_id) REFERENCES public.persona(cedula);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;
