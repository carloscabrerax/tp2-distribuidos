package py.una.pol.personas.model;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@SuppressWarnings("serial")
@XmlRootElement
public class Asignatura implements Serializable {

	private Long id;
	private String nombre;
	private List<Persona> alumnos;

	public Asignatura(){
		alumnos = new ArrayList<Persona>();
	}

	public Asignatura(Long pid, String pnombre){
		this.id = pid;
		this.nombre = pnombre;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Persona> getAlumnos() {
		return alumnos;
	}

	public void setAlumnos(List<Persona> alumnos) {
		this.alumnos = alumnos;
	}

}
