/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package py.una.pol.personas.rest;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import py.una.pol.personas.dao.AsignaturaDAO;
import py.una.pol.personas.dao.PersonaDAO;
import py.una.pol.personas.model.Asignatura;
import py.una.pol.personas.model.Persona;
import py.una.pol.personas.service.AsignaturaService;
import py.una.pol.personas.service.PersonaService;

/**
 * JAX-RS Example
 * <p/>
 * Esta clase produce un servicio RESTful para leer y escribir contenido de personas
 */
@Path("/personas")
@RequestScoped
public class PersonaRESTService {

    @Inject
    private Logger log;

    @Inject
    PersonaService personaService;

    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<Persona> listar() {
        return personaService.seleccionar();
    }

    @GET
    @Path("/{cedula:[0-9][0-9]*}")
    @Produces(MediaType.APPLICATION_JSON)
    public Persona obtenerPorId(@PathParam("cedula") long cedula) {
        Persona p = personaService.seleccionarPorCedula(cedula);
        if (p == null) {
        	log.info("obtenerPorId " + cedula + " no encontrado.");
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        log.info("obtenerPorId " + cedula + " encontrada: " + p.getNombre());
        return p;
    }

    @GET
    @Path("/cedula")
    @Produces(MediaType.APPLICATION_JSON)
    public Persona obtenerPorIdQuery(@QueryParam("cedula") long cedula) {
        Persona p = personaService.seleccionarPorCedula(cedula);
        if (p == null) {
         	log.info("obtenerPorId " + cedula + " no encontrado.");
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        log.info("obtenerPorId " + cedula + " encontrada: " + p.getNombre());
        return p;
    }

    /**
     * Creates a new member from the values provided. Performs validation, and will return a JAX-RS response with either 200 ok,
     * or with a map of fields, and related errors.
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crear(Persona p) {

        Response.ResponseBuilder builder = null;

        try {
            personaService.crear(p);
            // Create an "ok" response
            
            //builder = Response.ok();
            builder = Response.status(201).entity("Persona creada exitosamente");
            
        } catch (SQLException e) {
            // Handle the unique constrain violation
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("bd-error", e.getLocalizedMessage());
            builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }

        return builder.build();
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response actualizar(Persona p) {

        Response.ResponseBuilder builder = null;

        try {
            personaService.actualizar(p);
            // Create an "ok" response

            //builder = Response.ok();
            builder = Response.status(201).entity("Persona actualizada exitosamente");

        } catch (SQLException e) {
            // Handle the unique constrain violation
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("bd-error", e.getLocalizedMessage());
            builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }

        return builder.build();
    }

   @DELETE
   @Path("/{cedula}")
   public Response borrar(@PathParam("cedula") long cedula) {
	   Response.ResponseBuilder builder = null;
	   try {
		   if(personaService.seleccionarPorCedula(cedula) == null) {
			   
			   builder = Response.status(Response.Status.NOT_ACCEPTABLE).entity("Persona no existe.");
		   }else {
			   personaService.borrar(cedula);
			   builder = Response.status(202).entity("Persona borrada exitosamente.");			   
		   }
	   } catch (SQLException e) {
           // Handle the unique constrain violation
           Map<String, String> responseObj = new HashMap<>();
           responseObj.put("bd-error", e.getLocalizedMessage());
           builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
       } catch (Exception e) {
           // Handle generic exceptions
           Map<String, String> responseObj = new HashMap<>();
           responseObj.put("error", e.getMessage());
           builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
       }
       return builder.build();
   }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/asignatura")
    public Response asociarAsignatura(AsignaturaPersona ap) {

        Response.ResponseBuilder builder = null;
        AsignaturaDAO asignaturaDAO = new AsignaturaDAO();
        PersonaDAO personaDAO = new PersonaDAO();
        Persona p = personaDAO.seleccionarPorCedula(ap.getId_persona());
        Asignatura a = asignaturaDAO.seleccionarPorId(ap.getId_asignatura());
        try {

            personaService.asociarAsignatura(a, p, ap.getCalificacion());
            // Create an "ok" response
            //builder = Response.ok();
            builder = Response.status(201).entity("Asignatura asociada exitosamente");

        } catch (SQLException e) {
            // Handle the unique constrain violation
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("bd-error", e.getLocalizedMessage());
            builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }
        return builder.build();
    }

    @DELETE
    @Path("/{cedula}/asignaturas/{id}")
    public Response desasociarAsignatura(@PathParam("cedula") long pid, @PathParam("id") long aid) {
        Response.ResponseBuilder builder = null;
        try {
            if(personaService.seleccionarPorCedula(pid) == null) {

                builder = Response.status(Response.Status.NOT_ACCEPTABLE).entity("Persona no existe.");
            }else {
                personaService.desasociarAsignatura(pid, aid);
                builder = Response.status(202).entity("Persona borrada exitosamente.");
            }
        } catch (SQLException e) {

            // Handle the unique constrain violation
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("bd-error", e.getLocalizedMessage());
            builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }
        return builder.build();
    }

}


class AsignaturaPersona{
    private long id;
    private long id_asignatura;
    private long id_persona;
    private long calificacion;

    public long getId_asignatura() {
        return id_asignatura;
    }

    public void setId_asignatura(long id_asignatura) {
        this.id_asignatura = id_asignatura;
    }

    public long getId_persona() {
        return id_persona;
    }

    public void setId_persona(long id_persona) {
        this.id_persona = id_persona;
    }

    public long getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(long calificacion) {
        this.calificacion = calificacion;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}

